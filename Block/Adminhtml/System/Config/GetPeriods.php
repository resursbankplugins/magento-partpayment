<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Block\Adminhtml\System\Config;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Resursbank\Core\Helper\Config as CoreConfig;
use Resursbank\Core\Helper\Scope;
use Resursbank\Ecom\Config;
use Resursbank\Partpayment\Helper\Config as PartPaymentConfig;
use Resursbank\Ecom\Module\AnnuityFactor\Widget\GetPeriods as GetPeriodsWidget;
use Resursbank\Partpayment\Helper\Log;
use Throwable;

/**
 * Render widget utilised to fetch annuity periods in admin config.
 */
class GetPeriods extends Template
{
    /**
     * @param Context $context
     * @param Log $log
     * @param CoreConfig $coreConfig
     * @param Scope $scope
     * @param PartPaymentConfig $config
     * @param array $data
     */
    public function __construct(
        Context $context,
        private readonly Log $log,
        private readonly CoreConfig $coreConfig,
        private readonly Scope $scope,
        private readonly PartPaymentConfig $config,
        array $data = []
    ) {
        parent::__construct(
            context: $context,
            data: $data
        );
    }

    /**
     * Resolve widget content.
     *
     * @retrun string
     */
    public function getWidget(): string
    {
        try {
            Config::validateInstance();

            $widget = new GetPeriodsWidget(
                storeId: $this->coreConfig->getStore(
                    scopeCode: $this->scope->getId(),
                    scopeType: $this->scope->getType()
                ),
                methodElementId: 'payment_other_resursbank_section_partpayment_ecom_method',
                periodElementId: 'payment_other_resursbank_section_partpayment_ecom_months',
                automatic: false,
                selectedPaymentMethod: $this->config->getEcomMethod(
                    scopeCode: $this->scope->getId(),
                    scopeType: $this->scope->getType()
                ),
                selectedPeriod: (string)$this->config->getEcomMonths(
                    scopeCode: $this->scope->getId(),
                    scopeType: $this->scope->getType()
                )
            );

            return $widget->js;
        } catch (Throwable $error) {
            $this->log->exception(error: $error);
        }

        return '';
    }
}
