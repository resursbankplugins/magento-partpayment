<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Block\Frontend;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product as CatalogProduct;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Data\Form\FormKey;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Pricing\PriceInfo\Base;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\StoreManagerInterface;
use Resursbank\Core\Api\Data\PaymentMethodInterface;
use Resursbank\Core\Exception\InvalidDataException;
use Resursbank\Core\Helper\Ecom as CoreEcom;
use Resursbank\Ecom\Module\PaymentMethod\Widget\PartPayment;
use Resursbank\Ecom\Module\PaymentMethod\Widget\ReadMore;
use Resursbank\Ecom\Module\PaymentMethod\Widget\ReadMoreJs;
use Resursbank\Partpayment\Api\Data\AnnuityInterface;
use Resursbank\Partpayment\Helper\Annuity;
use Resursbank\Partpayment\Helper\Config;
use Resursbank\Partpayment\Helper\Ecom;
use Resursbank\Partpayment\Helper\Log;
use Resursbank\Partpayment\Helper\Method;
use Resursbank\Partpayment\Helper\Price;
use Throwable;

/**
 * Implementation of part payment widget for product pages.
 *
 * Contains implementation for both deprecated and modern API:s since they
 * share a lot of the same business logic. Anything related to annuity / method
 * data from the local database is only used for deprecated API implementations.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Product extends Template
{
    /**
     * Cache product data to avoid redundant database transactions.
     *
     * @var ProductInterface|null
     */
    private ?ProductInterface $product = null;

    /**
     * Cache current store scope code to avoid resolving it multiple times.
     *
     * @var string|null
     */
    private ?string $scopeCode = null;

    /**
     * Cache whether a modern API has been configured.
     *
     * @var bool|null
     */
    private ?bool $useEcom = null;

    /**
     * Cache payment method data to avoid redundant database transactions.
     *
     *  NOTE: This property is only used for deprecated API implementations.
     *
     * @var PaymentMethodInterface|null
     */
    private ?PaymentMethodInterface $method = null;

    /**
     * Cache  annuity information data to avoid redundant database transactions.
     *
     *  NOTE: This property is only used for deprecated API implementations.
     *
     * @var AnnuityInterface|null
     */
    private ?AnnuityInterface $annuity = null;

    /**
     * Widget instance for modern API:s.
     *
     * @var PartPayment|null
     */
    public readonly ?PartPayment $ecomWidget;

    /**
     * Read more widget associated with modern API widget.
     *
     * @var ReadMore|null
     */
    public readonly ?ReadMore $ecomRmWidget;

    public readonly ?ReadMoreJs $ecomReadMoreJsWidget;

    /**
     * @param Log $log
     * @param Config $config
     * @param FormKey $formKey
     * @param Price $price
     * @param Method $methodRepo
     * @param StoreManagerInterface $storeManager
     * @param Annuity $annuityRepo
     * @param CoreEcom $coreEcom
     * @param Ecom $ecom
     * @param RequestInterface $request
     * @param ProductRepositoryInterface $productRepository
     * @param Context $context
     * @param array $data
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        protected readonly Log $log,
        protected readonly Config $config,
        protected readonly FormKey $formKey,
        protected readonly Price $price,
        protected readonly Method $methodRepo,
        protected readonly StoreManagerInterface $storeManager,
        private readonly Annuity $annuityRepo,
        private readonly CoreEcom $coreEcom,
        public readonly Ecom $ecom,
        private readonly RequestInterface $request,
        private readonly ProductRepositoryInterface $productRepository,
        Context $context,
        array $data = []
    ) {
        try {
            if ($this->useEcom()) {
                $this->ecomWidget = $this->ecom->getWidget(
                    amount: $this->getPrice()
                );
                $this->ecomRmWidget = $this->ecom->getReadMoreWidget(
                    amount: $this->getPrice()
                );
                $this->ecomReadMoreJsWidget = new ReadMoreJs(
                    containerElDomPath: '#rb-pp-widget-container',
                    autoInitJs: true
                );
            }
        } catch (Throwable $e) {
            $this->log->exception(error: $e);
        }

        parent::__construct(context: $context, data: $data);
    }

    /**
     * Resolve template to be rendered based on API configuration.
     *
     * @return string
     */
    public function getTemplate(): string
    {
        try {
            return $this->useEcom() ?
                'Resursbank_Partpayment::product/ecom.phtml' :
                'Resursbank_Partpayment::product/deprecated.phtml';
        } catch (Throwable $e) {
            $this->log->exception($e);
        }

        return '';
    }

    /**
     * Only render if the feature is activated and threshold criteria is met.
     *
     * @return string
     */
    protected function _toHtml(): string
    {
        return $this->isActive() ? parent::_toHtml() : '';
    }

    /**
     * Returns the price of the product, or 0.0 if the product can't be found.
     *
     * @return float
     */
    public function getPrice(): float
    {
        $result = 0.0;

        try {
            $product = $this->getProduct();
            $result = (float) $product->getPrice();

            if ($product instanceof CatalogProduct) {
                $priceInfo = $product->getPriceInfo();

                if ($priceInfo instanceof Base) {
                    $result = (float) $priceInfo
                        ->getPrices()
                        ->get('final_price')
                        ->getAmount()
                        ->getValue();
                }
            }
        } catch (Throwable $e) {
            $this->log->exception($e);
        }

        return $result;
    }

    /**
     * Get product type.
     *
     * @return string
     */
    public function getProductType(): string
    {
        $result = '';

        try {
            // Avoid type casting since getTypeId may return an array.
            $result = $this->getProduct()->getTypeId();
        } catch (Throwable $e) {
            $this->log->exception($e);
        }

        return is_string($result) ? $result : '';
    }

    /**
     * Retrieve form key value, for AJAX requests.
     *
     *  NOTE: This method is only used for deprecated API implementations.
     *
     * @return string
     */
    public function getFormKey(): string
    {
        $result = '';

        try {
            $result = $this->formKey->getFormKey();
        } catch (Throwable $e) {
            $this->log->exception(error: $e);
        }

        return $result;
    }

    /**
     * Retrieve suggested part payment price based on Quote total.
     *
     *  NOTE: This method is only used for deprecated API implementations.
     *
     * @param float $price
     * @return float
     */
    public function getSuggestedPrice(
        float $price
    ): float {
        $result = 0.0;

        try {
            $method = $this->getMethod();
            $result = $this->price->getSuggestedPrice(
                method: $method,
                annuity: $this->getAnnuity(),
                price: $price
            );
        } catch (Throwable $e) {
            $this->log->exception(error: $e);
        }

        return $result;
    }

    /**
     * Retrieve duration of configured annuity to base calculations on.
     *
     *  NOTE: This method is only used for deprecated API implementations.
     *
     * @return int
     */
    public function getDuration(): int
    {
        $result = 0;

        try {
            $result = (int) $this->getAnnuity()->getDuration();
        } catch (Throwable $e) {
            $this->log->exception(error: $e);
        }

        return $result;
    }

    /**
     * Retrieve factor of configured annuity to base calculations on.
     *
     *  NOTE: This method is only used for deprecated API implementations.
     *
     * @return float
     */
    public function getFactor(): float
    {
        $result = 0.0;

        try {
            $result = (float) $this->getAnnuity()->getFactor();
        } catch (Throwable $e) {
            $this->log->exception(error: $e);
        }

        return $result;
    }

    /**
     * Get configured payment method.
     *
     * Retrieve configured payment method to base calculations on. Store in
     * local variable to suppress redundant database transactions.
     *
     *  NOTE: This method is only used for deprecated API implementations.
     *
     * @return PaymentMethodInterface
     * @throws InvalidDataException
     */
    public function getMethod(): PaymentMethodInterface
    {
        // Resolve from cache.
        if ($this->method !== null) {
            return $this->method;
        }

        // Resolve from database.
        $this->method = $this->methodRepo->getMethod(frontend: true);

        if ($this->method === null) {
            throw new InvalidDataException(
                phrase: __('Please configure payment method to base prices on')
            );
        }

        return $this->method;
    }

    /**
     * Get configured annuity.
     *
     * Retrieve configured annuity to base calculations on. Store in local
     * variable to suppress redundant database transactions.
     *
     *  NOTE: This method is only used for deprecated API implementations.
     *
     * @return AnnuityInterface
     * @throws InvalidDataException|NoSuchEntityException
     */
    public function getAnnuity(): AnnuityInterface
    {
        // Resolve from cache.
        if ($this->annuity !== null) {
            return $this->annuity;
        }

        // Resolve from database.
        $this->annuity = $this->annuityRepo->getAnnuity(
            scopeCode: $this->getScopeCode()
        );

        if ($this->annuity === null) {
            throw new InvalidDataException(
                phrase: __('Please configure duration to base prices on.')
            );
        }

        return $this->annuity;
    }

    /**
     * Get product.
     *
     * @return ProductInterface
     * @throws InvalidDataException
     * @throws NoSuchEntityException
     */
    private function getProduct(): ProductInterface
    {
        // Resolve from cache.
        if ($this->product !== null) {
            return $this->product;
        }

        // Resolve from database.
        $productId = (int)$this->request->getParam('id');

        if ($productId > 0) {
            $storeId = $this->storeManager->getStore()->getId();

            // Cache to avoid redundant database transactions.
            $this->product = $this->productRepository->getById(
                productId: $productId,
                editMode: false,
                storeId: is_string($storeId) ? (int)$storeId : null
            );
        }

        if ($this->product === null) {
            throw new InvalidDataException(__('Unable to load product.'));
        }

        return $this->product;
    }

    /**
     * Resolve current store scope code.
     *
     * @return string
     * @throws NoSuchEntityException
     */
    private function getScopeCode(): string
    {
        if ($this->scopeCode === null) {
            $this->scopeCode = $this->storeManager->getStore()->getCode();
        }

        return $this->scopeCode;
    }

    /**
     * Checks whether a modern API integration is configured.
     *
     * @return bool
     * @throws NoSuchEntityException
     */
    private function useEcom(): bool
    {
        if ($this->useEcom === null) {
            $this->useEcom = $this->coreEcom->canConnect(
                scopeCode: $this->getScopeCode()
            );
        }

        return $this->useEcom;
    }

    /**
     * Check whether module is active.
     *
     * @return bool
     */
    private function isActive(): bool
    {
        $result = false;

        try {
            // Validate feature is enabled in config.
            if (!$this->config->isActive(scopeCode: $this->getScopeCode())) {
                return false;
            }

            // Validate configured threshold price.
            $price = $this->getPrice();

            if ($this->useEcom()) {
                // If the widget failed to render we can't display it.
                $result = $this->ecomWidget !== null;
            } else {
                $result = $this->price->isValid(
                    method: $this->getMethod(),
                    annuity: $this->getAnnuity(),
                    price: $price
                );
            }
        } catch (Throwable $e) {
            $this->log->exception(error: $e);
        }

        return $result;
    }
}
