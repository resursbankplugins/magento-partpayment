<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Controller\Frontend;

use JsonException;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use ReflectionException;
use Resursbank\Ecom\Exception\AttributeCombinationException;
use Resursbank\Ecom\Exception\ConfigException;
use Resursbank\Ecom\Lib\Model\PaymentMethod\PartPayment\InfoResponse;
use Resursbank\Ecom\Module\PaymentMethod\Http\PartPayment\InfoControllerInterface;
use Resursbank\Partpayment\Helper\Ecom;
use Resursbank\Partpayment\Helper\Log;
use Throwable;

/**
 * Fetch part payment widget information based on amount.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Info implements HttpPostActionInterface, InfoControllerInterface
{
    /**
     * @param Ecom $ecom
     * @param ResultFactory $resultFactory
     * @param RequestInterface $request
     * @param Log $log
     */
    public function __construct(
        private readonly Ecom $ecom,
        private readonly ResultFactory $resultFactory,
        private readonly RequestInterface $request,
        private readonly Log $log
    ) {
    }

    /**
     * Execute request.
     *
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        try {
            $amount = (float)$this->request->getParam(key: 'amount');

            if ($amount > 0) {
                $result->setData(
                    $this->getResponse(amount: $amount)->toArray()
                );
            } else {
                $result->setData(data: ['error' => __('Invalid amount.')]);
            }
        } catch (Throwable $error) {
            $this->log->exception(error: $error);
        }

        return $result;
    }

    /**
     * Resolve response data.
     *
     * @param float $amount
     * @return InfoResponse
     * @throws AttributeCombinationException
     * @throws ConfigException
     * @throws JsonException
     * @throws ReflectionException
     */
    public function getResponse(float $amount): InfoResponse
    {
        $widget = $this->ecom->getWidget(
            amount: $amount
        );

        return new InfoResponse(
            startingAt: (float) $widget?->cost->monthlyCost,
            html: (string) $widget?->content,
            readMoreHtml: (string) $this->ecom->getReadMoreWidget(
                amount: $amount
            )?->content,
        );
    }
}
