<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Helper;

use JsonException;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Locale\CurrencyInterface;
use Magento\Framework\UrlInterface;
use ReflectionException;
use Resursbank\Core\Helper\Config as CoreConfig;
use Resursbank\Core\Helper\Scope;
use Resursbank\Ecom\Exception\ApiException;
use Resursbank\Ecom\Exception\AuthException;
use Resursbank\Ecom\Exception\CacheException;
use Resursbank\Ecom\Exception\ConfigException;
use Resursbank\Ecom\Exception\CurlException;
use Resursbank\Ecom\Exception\Validation\EmptyValueException;
use Resursbank\Ecom\Exception\Validation\IllegalTypeException;
use Resursbank\Ecom\Exception\Validation\IllegalValueException;
use Resursbank\Ecom\Exception\ValidationException;
use Resursbank\Ecom\Lib\Model\PaymentMethod;
use Resursbank\Ecom\Module\PaymentMethod\Enum\CurrencyFormat;
use Resursbank\Ecom\Module\PaymentMethod\Repository;
use Resursbank\Ecom\Module\PaymentMethod\Widget\PartPayment;
use Resursbank\Ecom\Module\PaymentMethod\Widget\ReadMore;
use Throwable;

/**
 * Ecom related business logic.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Ecom extends AbstractHelper
{
    /**
     * @param Log $log
     * @param Config $config
     * @param Scope $scope
     * @param CoreConfig $coreConfig
     * @param CurrencyInterface $currency
     * @param UrlInterface $url
     * @param Context $context
     */
    public function __construct(
        private readonly Log $log,
        private readonly Config $config,
        private readonly Scope $scope,
        private readonly CoreConfig $coreConfig,
        private readonly CurrencyInterface $currency,
        private readonly UrlInterface $url,
        Context $context
    ) {
        parent::__construct(context: $context);
    }

    /**
     * Render the part payment widget.
     *
     * This is centralized here because we also render the widget when we need
     * to resolve starting cost.
     *
     * @param float $amount
     * @return PartPayment|string
     */
    public function getWidget(
        float $amount
    ): ?PartPayment {
        try {
            if ($amount <= 0) {
                return null;
            }

            $store = $this->scope->getStoreManager()->getStore();
            $scopeCode = $store->getCode();
            $currency = $this->currency->getCurrency(
                $store->getCurrentCurrencyCode()
            );
            $storeId = $this->coreConfig->getStore(scopeCode: $scopeCode);
            $method = $this->getPaymentMethod();
            $months = $this->config->getEcomMonths(scopeCode: $scopeCode);

            if ($method === null || $months === 0) {
                return null;
            }

            return new PartPayment(
                storeId: $storeId,
                paymentMethod: $method,
                months: $months,
                amount: $amount,
                currencySymbol: $currency->getSymbol(),
                currencyFormat: CurrencyFormat::SYMBOL_LAST,
                fetchStartingCostUrl: $this->url->getUrl(
                    'resursbank_partpayment/frontend/info'
                ),
                threshold: $this->config->getThreshold(scopeCode: $scopeCode)
            );
        } catch (Throwable $e) {
            $this->log->exception($e);
        }

        return null;
    }

    /**
     * Generate read more instance.
     *
     * @param float $amount
     * @return ReadMore|null
     */
    public function getReadMoreWidget(
        float $amount
    ): ?ReadMore {
        try {
            if ($amount <= 0) {
                return null;
            }

            $method = $this->getPaymentMethod();

            if ($method === null) {
                return null;
            }

            return new ReadMore(
                paymentMethod: $method,
                amount: $amount
            );
        } catch (Throwable $e) {
            $this->log->exception($e);
        }

        return null;
    }

    /**
     * Get the payment method.
     *
     * @return PaymentMethod
     * @throws Throwable
     * @throws JsonException
     * @throws NoSuchEntityException
     * @throws ReflectionException
     * @throws ApiException
     * @throws AuthException
     * @throws CacheException
     * @throws ConfigException
     * @throws CurlException
     * @throws ValidationException
     * @throws EmptyValueException
     * @throws IllegalTypeException
     * @throws IllegalValueException
     */
    private function getPaymentMethod(): ?PaymentMethod
    {
        $store = $this->scope->getStoreManager()->getStore();
        $scopeCode = $store->getCode();
        $method = $this->config->getEcomMethod(scopeCode: $scopeCode);

        if ($method === '') {
            return null;
        }

        return Repository::getById(
            paymentMethodId: $method
        );
    }
}
