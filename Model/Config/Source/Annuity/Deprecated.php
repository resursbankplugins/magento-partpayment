<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

namespace Resursbank\Partpayment\Model\Config\Source\Annuity;

use Exception;
use Magento\Framework\Data\OptionSourceInterface;
use Resursbank\Partpayment\Helper\Log;
use Resursbank\Partpayment\Helper\Annuity as AnnuityHelper;
use Resursbank\Partpayment\Helper\Method as MethodHelper;

class Deprecated implements OptionSourceInterface
{
    /**
     * @param Log $log
     * @param AnnuityHelper $annuityHelper
     * @param MethodHelper $methodHelper
     */
    public function __construct(
        private readonly Log $log,
        private readonly AnnuityHelper $annuityHelper,
        private readonly MethodHelper $methodHelper
    ) {
    }

    /**
     * @inheritDoc
     *
     * @return array<array>
     */
    public function toOptionArray(): array
    {
        $result = [
            [
                'value' => '',
                'label' => __('rb-please-select')
            ]
        ];

        foreach ($this->toArray() as $id => $title) {
            $result[] = [
                'value' => $id,
                'label' => $title
            ];
        }

        return $result;
    }

    /**
     * Convert object to array.
     *
     * @return array<int, string>
     */
    public function toArray(): array
    {
        $result = [];

        try {
            $method = $this->methodHelper->getMethod(false);

            if ($method !== null) {
                $annuities = $this->annuityHelper->getListByMethod($method);

                foreach ($annuities as $annuity) {
                    $annuityId = $annuity->getAnnuityId();
                    $title = $annuity->getTitle();

                    if ($annuityId !== null && $title !== null) {
                        $result[$annuityId] = $title;
                    }
                }
            }
        } catch (Exception $e) {
            $this->log->exception($e);
        }

        return $result;
    }
}
