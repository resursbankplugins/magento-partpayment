<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

namespace Resursbank\Partpayment\Model\Config\Source\Method;

use Exception;
use Magento\Framework\Data\OptionSourceInterface;
use Resursbank\Core\Helper\PaymentMethods;
use Resursbank\Core\Helper\Scope;
use Resursbank\Partpayment\Helper\Log;
use Resursbank\Partpayment\Helper\Method as MethodHelper;

class Deprecated implements OptionSourceInterface
{
    /**
     * @param Log $log
     * @param MethodHelper $method
     * @param PaymentMethods $paymentMethods
     * @param Scope $scope
     */
    public function __construct(
        private readonly Log $log,
        private readonly MethodHelper $method,
        private readonly PaymentMethods $paymentMethods,
        private readonly Scope $scope
    ) {
    }

    /**
     * @inheritDoc
     *
     * @return array<array>
     */
    public function toOptionArray(): array
    {
        $result = [
            [
                'value' => '',
                'label' => __('rb-please-select')
            ]
        ];

        foreach ($this->toArray() as $id => $title) {
            $result[] = [
                'value' => $id,
                'label' => $title
            ];
        }

        return $result;
    }

    /**
     * Convert object to array.
     *
     * @return array<int, string>
     */
    public function toArray(): array
    {
        $result = [];

        try {
            $methods = $this->paymentMethods->getMethodsByCredentials(
                $this->scope->getId(),
                $this->scope->getType()
            );

            foreach ($methods as $method) {
                if ($this->method->isEligible($method)) {
                    $methodId = $method->getMethodId();
                    $title = $method->getTitle();

                    if ($methodId !== null && $title !== null) {
                        $result[$methodId] = $title;
                    }
                }
            }
        } catch (Exception $e) {
            $this->log->exception($e);
        }

        return $result;
    }
}
