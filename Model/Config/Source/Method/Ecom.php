<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

namespace Resursbank\Partpayment\Model\Config\Source\Method;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Ecom method source.
 *
 * This class returns empty arrays as the actual loading of options is handled
 * by the Part Payment configuration widget in Ecom.
 */
class Ecom implements OptionSourceInterface
{
    /**
     * @inheritDoc
     *
     * @return array<array>
     */
    public function toOptionArray(): array
    {
        return [];
    }

    /**
     * Convert object to array.
     *
     * @return array
     */
    public function toArray(): array
    {
        return [];
    }
}
