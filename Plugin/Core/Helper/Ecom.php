<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Plugin\Core\Helper;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Resursbank\Core\Helper\Ecom as Subject;
use Resursbank\Partpayment\Helper\Config;
use Resursbank\Core\Helper\Scope;
use Resursbank\Core\Helper\Version;

/**
 * Plugin for Resursbank\Core\Helper\Ecom.
 */
class Ecom
{
    /**
     * @param Config $config
     * @param Scope $scope
     * @param Version $version
     */
    public function __construct(
        private readonly Config $config,
        private readonly Scope $scope,
        private readonly Version $version
    ) {
    }

    /**
     * Append our module's name to the user-agent string.
     *
     * @param Subject $subject
     * @param string $result
     * @return string
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetUserAgent(
        Subject $subject,
        string $result
    ): string {
        if ($this->config->isActive(
            scopeCode: $this->scope->getId(),
            scopeType: $this->scope->getType()
        )) {
            return
                $result . ' Resursbank_Partpayment ' .
                $this->version->getComposerVersion(
                    module: 'Resursbank_Partpayment'
                ) . ' |';
        }

        return $result;
    }
}
