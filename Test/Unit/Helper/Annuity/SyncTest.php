<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Test\Unit\Helper\Annuity;

use PHPUnit\Framework\TestCase;
use stdClass;
use Resursbank\Partpayment\Helper\Annuity;
use Exception;
use function is_array;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\IntegrationException;
use Resursbank\Core\Api\Data\PaymentMethodInterface;
use Resursbank\Core\Helper\Api;
use Resursbank\Core\Model\Api\Credentials;
use Resursbank\Partpayment\Api\Data\AnnuityInterface;
use Resursbank\Partpayment\Helper\Log;
use Resursbank\Partpayment\Helper\Annuity\Converter;
use Resursbank\Partpayment\Helper\Annuity\Sync;

/**
 * Unit tests for Sync.
 */
class SyncTest extends TestCase
{
    /** @var Sync */
    private Sync $sync;

    /**
     * @inheritDoc
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->sync = new Sync(
            context: $this->createMock(originalClassName: Context::class),
            log: $this->createMock(originalClassName: Log::class),
            api: $this->createMock(originalClassName: Api::class),
            converter: $this->createMock(originalClassName: Converter::class),
            annuity: $this->createMock(originalClassName: Annuity::class)
        );
    }

    /**
     * Verify that valid data types are handled properly.
     *
     * @return void
     * @throws IntegrationException
     */
    public function testResolveAnnuityDataArray(): void
    {
        static::assertEquals(
            expected: gettype([]),
            actual: gettype($this->sync->resolveAnnuityDataArray(
                data: new stdClass()
            ))
        );
        static::assertEquals(
            expected: gettype([]),
            actual: gettype($this->sync->resolveAnnuityDataArray(
                data: []
            ))
        );
    }

    /**
     * Verify that method throws exception when input data type is incorrect.
     *
     * @return void
     * @throws IntegrationException
     */
    public function testResolveAnnuityDataArrayThrows(): void
    {
        static::expectException(IntegrationException::class);
        $this->sync->resolveAnnuityDataArray(
            data: new Exception(message: 'foo')
        );
    }
}
