<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Test\Unit\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Resursbank\Partpayment\Helper\Config;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class ConfigTest extends TestCase
{
    /**
     * @var Config
     */
    private Config $config;
    /**
     * @var ScopeConfigInterface|mixed|MockObject
     */
    private $scopeConfigInterfaceMock;

    /**
     * @inheriDoc
     */
    protected function setUp(): void
    {
        $contextMock = $this->createMock(Context::class);
        $writerInterfaceMock = $this->createMock(WriterInterface::class);
        $this->scopeConfigInterfaceMock = $this->createMock(ScopeConfigInterface::class);

        $this->config = new Config(
            $this->scopeConfigInterfaceMock,
            $writerInterfaceMock,
            $contextMock
        );
    }

    /**
     * Assert that IsActive return the correct value.
     */
    public function testIsActive(): void
    {
        $this->scopeConfigInterfaceMock->method('isSetFlag')->withConsecutive(
            ['resursbank/partpayment/active'],
            ['resursbank/partpayment/active']
        )->willReturn(true, false);
        self::assertTrue($this->config->isActive(''));
        self::assertFalse($this->config->isActive(''));
    }

    /**
     * Assert that IsActive returns true on store specific store if enabled.
     */
    public function testIsActiveReturnsTrueForSpecificStore(): void
    {
        $this->scopeConfigInterfaceMock
            ->method('isSetFlag')
            ->with('resursbank/partpayment/active', ScopeInterface::SCOPE_STORE, 'en')
            ->willReturn(true);

        self::assertTrue($this->config->isActive('en'));
    }

    /**
     * Assert that IsActive returns false if disabled on specific store.
     */
    public function testIsActiveReturnsFalseForSpecificStore(): void
    {
        $this->scopeConfigInterfaceMock
            ->method('isSetFlag')
            ->with('resursbank/partpayment/active', ScopeInterface::SCOPE_STORE, 'se')
            ->willReturn(false);

        self::assertFalse($this->config->isActive('se'));
    }

    /**
     * Assert that IsActive returns true for specific store if disabled on others.
     */
    public function testIsActiveReturnsFalseForSpecificStoreIfEnableOnOther(): void
    {
        $this->scopeConfigInterfaceMock->method('isSetFlag')->withConsecutive(
            ['resursbank/partpayment/active', ScopeInterface::SCOPE_STORE, 'en'],
            ['resursbank/partpayment/active', ScopeInterface::SCOPE_STORE, 'se'],
        )->willReturnOnConsecutiveCalls(true, false);

        self::assertTrue($this->config->isActive('en'));
        self::assertFalse($this->config->isActive('se'));
    }

    /**
     * Assert that isDebugEnabled return the correct value.
     */
    public function testIsDebugEnabled(): void
    {
        $this->scopeConfigInterfaceMock->method('isSetFlag')->withConsecutive(
            ['resursbank/partpayment/debug'],
            ['resursbank/partpayment/debug']
        )->willReturn(true, false);
        self::assertTrue($this->config->isDebugEnabled(''));
        self::assertFalse($this->config->isDebugEnabled(''));
    }

    /**
     * Assert that IsDebugEnabled returns true on store specific store if enabled.
     */
    public function testIsDebugEnabledReturnsTrueForSpecificStore(): void
    {
        $this->scopeConfigInterfaceMock
            ->method('isSetFlag')
            ->with('resursbank/partpayment/debug', ScopeInterface::SCOPE_STORE, 'en')
            ->willReturn(true);

        self::assertTrue($this->config->isDebugEnabled('en'));
    }

    /**
     * Assert that IsDebugEnabled returns false if disabled on specific store.
     */
    public function testIsDebugEnabledReturnsFalseForSpecificStore(): void
    {
        $this->scopeConfigInterfaceMock
            ->method('isSetFlag')
            ->with('resursbank/partpayment/debug', ScopeInterface::SCOPE_STORE, 'se')
            ->willReturn(false);

        self::assertFalse($this->config->isDebugEnabled('se'));
    }

    /**
     * Assert that IsDebugEnabled returns true for specific store if disabled on others.
     */
    public function testIsDebugEnabledReturnsFalseForSpecificStoreIfEnableOnOther(): void
    {
        $this->scopeConfigInterfaceMock->method('isSetFlag')->withConsecutive(
            ['resursbank/partpayment/debug', ScopeInterface::SCOPE_STORE, 'en'],
            ['resursbank/partpayment/debug', ScopeInterface::SCOPE_STORE, 'se'],
        )->willReturnOnConsecutiveCalls(true, false);

        self::assertTrue($this->config->isDebugEnabled('en'));
        self::assertFalse($this->config->isDebugEnabled('se'));
    }

    /**
     * Assert that GetMethod return the correct value.
     */
    public function testGetMethod(): void
    {
        $this->scopeConfigInterfaceMock
            ->method('getValue')
            ->with('resursbank/partpayment/method', ScopeInterface::SCOPE_STORE, 'en')
            ->willReturn('1');
        self::assertSame(1, $this->config->getMethod('en'));
    }

    /**
     * Assert that GetMethod returns true on store specific store if enabled.
     */
    public function testGetMethodReturnsCorrectValueForSpecificStore(): void
    {
        $this->scopeConfigInterfaceMock
            ->method('getValue')
            ->with('resursbank/partpayment/method', ScopeInterface::SCOPE_STORE, 'en')
            ->willReturn('1');

        self::assertSame(1, $this->config->getMethod('en'));
    }

    /**
     * Assert that GetMethod returns true for specific store if disabled on others.
     */
    public function testGetMethodReturnsCorrectValueForSpecificStores(): void
    {
        $this->scopeConfigInterfaceMock->method('getValue')->withConsecutive(
            ['resursbank/partpayment/method', ScopeInterface::SCOPE_STORE, 'en'],
            ['resursbank/partpayment/method', ScopeInterface::SCOPE_STORE, 'se'],
        )->willReturnOnConsecutiveCalls('1', '2');

        self::assertSame(1, $this->config->getMethod('en'));
        self::assertSame(2, $this->config->getMethod('se'));
    }
}
