<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Test\Unit\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Resursbank\Core\Api\Data\PaymentMethodInterface;
use Resursbank\Partpayment\Api\Data\AnnuityInterface;
use Resursbank\Partpayment\Helper\Log;
use Resursbank\Partpayment\Helper\Price;
use Resursbank\Partpayment\Helper\Config;

class PriceTest extends TestCase
{
    /**
     * @var PaymentMethodInterface|MockObject
     */
    private $paymentMethodMock;

    /**
     * @var AnnuityInterface|MockObject
     */
    private $annuityMock;

    /**
     * @var Price
     */
    private Price $price;

    /**
     * @inheriDoc
     */
    public function setUp(): void
    {
        $contextMock = $this->createMock(Context::class);
        $logMock = $this->createMock(Log::class);
        $priceCurrencyMock = $this->createMock(PriceCurrencyInterface::class);
        $this->paymentMethodMock = $this->createMock(PaymentMethodInterface::class);
        $this->annuityMock = $this->createMock(AnnuityInterface::class);
        $storeManagerInterfaceMock = $this->createMock(originalClassName: StoreManagerInterface::class);
        $mockStore = $this->createMock(originalClassName: StoreInterface::class);
        $storeManagerInterfaceMock
            ->expects($this->any())
            ->method('getStore')
            ->willReturn($mockStore);
        $configMock = $this->createMock(originalClassName: Config::class);
        $this->price = new Price(
            $contextMock,
            $logMock,
            $priceCurrencyMock,
            $storeManagerInterfaceMock,
            $configMock
        );
    }

    /**
     * Assert correct price is returned.
     */
    public function testCalculateReturnsCorrectPrice(): void
    {
        $this->annuityMock->expects(self::exactly(2))->method('getFactor')->willReturn(0.5);
        $this->annuityMock->expects(self::exactly(2))->method('getDuration')->willReturn(6);
        $this->paymentMethodMock->expects(self::once())->method('getMinOrderTotal')->willReturn(123.00);
        $this->paymentMethodMock->expects(self::once())->method('getMaxOrderTotal')->willReturn(123456.00);

        self::assertEquals(617, $this->price->calculate($this->paymentMethodMock, $this->annuityMock, 1234.00));
    }

    /**
     * Assert the value returned is rounded to 2 decimals.
     */
    public function testCalculateReturnsCorrectAmountOfDecimals(): void
    {
        // A factor of 0.4578 and duration of 6 will result in a 564,9252
        $this->annuityMock->expects(self::exactly(2))->method('getFactor')->willReturn(0.4578);
        $this->annuityMock->expects(self::exactly(2))->method('getDuration')->willReturn(6);
        $this->paymentMethodMock->expects(self::once())->method('getMinOrderTotal')->willReturn(123.00);
        $this->paymentMethodMock->expects(self::once())->method('getMaxOrderTotal')->willReturn(123456.00);

        self::assertEquals(564.93, $this->price->calculate($this->paymentMethodMock, $this->annuityMock, 1234.00));
    }

    /**
     * Assert the value returned is zero if getFactor return less than zero value.
     */
    public function testCalculateReturnsZeroValueIfFactorIsLessThanZero(): void
    {
        $this->annuityMock->expects(self::once())->method('getFactor')->willReturn(-0.5);
        $this->annuityMock->expects(self::never())->method('getDuration');
        $this->paymentMethodMock->expects(self::never())->method('getMinOrderTotal');
        $this->paymentMethodMock->expects(self::never())->method('getMaxOrderTotal');

        self::assertEquals(0, $this->price->calculate($this->paymentMethodMock, $this->annuityMock, 1234.00));
    }

    /**
     * Assert isValid returns true for valid parameters.
     */
    public function testIsValidReturnsTrue(): void
    {
        $this->annuityMock->expects(self::exactly(2))->method('getDuration')->willReturn(6);
        $this->paymentMethodMock->expects(self::once())->method('getMinOrderTotal')->willReturn(123.00);
        $this->paymentMethodMock->expects(self::once())->method('getMaxOrderTotal')->willReturn(12345.00);

        self::assertTrue($this->price->isValid($this->paymentMethodMock, $this->annuityMock, 1234.00));
    }

    /**
     * Assert isValid returns false if price is higher than max order total.
     */
    public function testIsValidReturnsFalseWhenPriceTooHigh(): void
    {
        $this->annuityMock->expects(self::exactly(2))->method('getDuration')->willReturn(6);
        $this->paymentMethodMock->expects(self::once())->method('getMinOrderTotal')->willReturn(123.00);
        $this->paymentMethodMock->expects(self::once())->method('getMaxOrderTotal')->willReturn(12345.00);

        self::assertFalse($this->price->isValid($this->paymentMethodMock, $this->annuityMock, 12345.50));
    }

    /**
     * Assert isValid returns false if calculated price is less than min order total.
     */
    public function testIsValidReturnsFalseWhenPriceTooLow(): void
    {
        $this->annuityMock->expects(self::exactly(2))->method('getDuration')->willReturn(11);
        $this->paymentMethodMock->expects(self::once())->method('getMinOrderTotal')->willReturn(123.00);
        $this->paymentMethodMock->expects(self::never())->method('getMaxOrderTotal');

        self::assertFalse($this->price->isValid($this->paymentMethodMock, $this->annuityMock, 1234.00));/**/
    }
}
