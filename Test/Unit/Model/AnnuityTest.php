<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Test\Unit\Model;

use JsonException;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Exception\ValidatorException;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Resursbank\Partpayment\Model\Annuity;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 */
class AnnuityTest extends TestCase
{
    /**
     * @var Context|MockObject
     */
    private $contextMock;

    /**
     * @var Registry|MockObject
     */
    private $registryMock;

    /**
     * @var AbstractResource|MockObject
     */
    private $resourceMock;

    /**
     * @var AbstractDb|MockObject
     */
    private $resourceCollectionMock;

    /**
     * @var Annuity
     */
    private Annuity $annuity;

    /**
     * Main set up method
     */
    public function setUp(): void
    {
        $this->contextMock = $this->createMock(Context::class);
        $this->registryMock = $this->createMock(Registry::class);
        $this->resourceMock = $this->getMockBuilder(AbstractResource::class)
            ->addMethods(['getIdFieldName'])
            ->onlyMethods(['getConnection'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $this->resourceCollectionMock = $this->createMock(AbstractDb::class);
        $this->annuity = new Annuity(
            $this->contextMock,
            $this->registryMock,
            $this->resourceMock,
            $this->resourceCollectionMock
        );
    }

    /**
     * Assert that the value of getAnnuityId is an integer.
     */
    public function testGetAnnuityIdReturnsInt(): void
    {
        $annuity = new Annuity(
            $this->contextMock,
            $this->registryMock,
            $this->resourceMock,
            $this->resourceCollectionMock,
            ['annuity_id' => '1']
        );
        self::assertIsInt($annuity->getAnnuityId());
        self::assertEquals(1, $annuity->getAnnuityId());
    }

    /**
     * Assert that the value of getAnnuityId is null for newly created model.
     */
    public function testGetAnnuityIdReturnsNullForANewlyCreatedModel(): void
    {
        self::assertNull($this->annuity->getAnnuityId());
    }

    /**
     * Assert that setAnnuityId sets the annuity_id.
     * @throws ValidatorException
     */
    public function testSetAnnuityIdSetsTheValue(): void
    {
        $this->annuity->setAnnuityId(2);
        self::assertEquals(2, $this->annuity->getAnnuityId());
    }

    /**
     * Assert that setAnnuityId throws exception when supplying a negative parameter.
     */
    public function testSetAnnuityIdThrowsExceptionIfParameterIsNegativeInteger(): void
    {
        $this->expectException(ValidatorException::class);
        $this->expectErrorMessage('Annuity ID must be be an integer that\'s more or equal ' .
            'to 0, or null. Use null to create a new database entry.');
        self::assertNull($this->annuity->setAnnuityId(-1));
    }

    /**
     * Assert that the value of getMethodId is an integer.
     */
    public function testGetMethodIdReturnsInt(): void
    {
        $annuity = new Annuity(
            $this->contextMock,
            $this->registryMock,
            $this->resourceMock,
            $this->resourceCollectionMock,
            ['method_id' => '1']
        );
        self::assertIsInt($annuity->getMethodId());
        self::assertEquals(1, $annuity->getMethodId());
    }

    /**
     * Assert that the value of getMethodId is null for newly created model.
     */
    public function testGetMethodIdReturnsNullForANewlyCreatedModel(): void
    {
        self::assertNull($this->annuity->getMethodId());
    }

    /**
     * Assert that setMethodId sets the method_id.
     * @throws ValidatorException
     */
    public function testSetMethodIdSetsTheValue(): void
    {
        $this->annuity->setMethodId(2);
        self::assertEquals(2, $this->annuity->getMethodId());
    }

    /**
     * Assert that setMethodId throws exception when supplying a negative parameter.
     */
    public function testSetMethodIdThrowsExceptionIfParameterIsNegativeInteger(): void
    {
        $this->expectException(ValidatorException::class);
        $this->expectErrorMessage('Method ID value may not be negative.');
        self::assertNull($this->annuity->setMethodId(-1));
    }

    /**
     * Assert that the value of getDuration is an integer.
     */
    public function testGetDurationReturnsInt(): void
    {
        $annuity = new Annuity(
            $this->contextMock,
            $this->registryMock,
            $this->resourceMock,
            $this->resourceCollectionMock,
            ['duration' => '1']
        );
        self::assertIsInt($annuity->getDuration());
        self::assertEquals(1, $annuity->getDuration());
    }

    /**
     * Assert that the value of getDuration is null for newly created model.
     */
    public function testGetDurationReturnsNullForANewlyCreatedModel(): void
    {
        self::assertNull($this->annuity->getDuration());
    }

    /**
     * Assert that setDuration sets the duration.
     * @throws ValidatorException
     */
    public function testSetDurationSetsTheValue(): void
    {
        $this->annuity->setDuration(2);
        self::assertEquals(2, $this->annuity->getDuration());
    }

    /**
     * Assert that setDuration throws exception when supplying a negative parameter.
     */
    public function testSetDurationThrowsExceptionIfParameterIsNegativeInteger(): void
    {
        $this->expectException(ValidatorException::class);
        $this->expectErrorMessage('Duration value may not be negative.');
        self::assertNull($this->annuity->setDuration(-1));
    }

    /**
     * Assert that the value of getFactor is an integer.
     */
    public function testGetFactorReturnsInt(): void
    {
        $annuity = new Annuity(
            $this->contextMock,
            $this->registryMock,
            $this->resourceMock,
            $this->resourceCollectionMock,
            ['factor' => '1']
        );
        self::assertIsFloat($annuity->getFactor());
        self::assertEquals(1, $annuity->getFactor());
    }

    /**
     * Assert that the value of getFactor is null for newly created model.
     */
    public function testGetFactorReturnsNullForANewlyCreatedModel(): void
    {
        self::assertNull($this->annuity->getFactor());
    }

    /**
     * Assert that setFactor sets the factor.
     * @throws ValidatorException
     */
    public function testSetFactorSetsTheValue(): void
    {
        $this->annuity->setFactor(2);
        self::assertEquals(2, $this->annuity->getFactor());
    }

    /**
     * Assert that setFactor sets the factor.
     * @throws ValidatorException
     */
    public function testSetFactorSetsAFloatValue(): void
    {
        $this->annuity->setFactor(2);
        self::assertIsFloat($this->annuity->getFactor());
        self::assertEquals(2.0, $this->annuity->getFactor());
    }

    /**
     * Assert that setFactor throws exception when supplying a negative parameter.
     */
    public function testSetFactorThrowsExceptionIfParameterIsNegativeInteger(): void
    {
        $this->expectException(ValidatorException::class);
        $this->expectErrorMessage('Factor value may not be negative.');
        self::assertNull($this->annuity->setFactor(-1));
    }

    /**
     * Assert that the value of getTitle is a string.
     */
    public function testGetTitleReturnsString(): void
    {
        $annuity = new Annuity(
            $this->contextMock,
            $this->registryMock,
            $this->resourceMock,
            $this->resourceCollectionMock,
            ['title' => 'Title']
        );
        self::assertIsString($annuity->getTitle());
        self::assertEquals('Title', $annuity->getTitle());
    }

    /**
     * Assert that the value of getTitle is null for newly created model.
     */
    public function testGetTitleReturnsNullForANewlyCreatedModel(): void
    {
        self::assertNull($this->annuity->getTitle());
    }

    /**
     * Assert that setTitle sets the title.
     */
    public function testSetTitleSetsTheValue(): void
    {
        $this->annuity->setTitle('Title');
        self::assertEquals('Title', $this->annuity->getTitle());
    }

    /**
     * Assert that the value of getRaw is a string.
     */
    public function testGetRawReturnsString(): void
    {
        $annuity = new Annuity(
            $this->contextMock,
            $this->registryMock,
            $this->resourceMock,
            $this->resourceCollectionMock,
            ['raw' => '{"key":"value"}']
        );
        self::assertIsString($annuity->getRaw());
        self::assertEquals('{"key":"value"}', $annuity->getRaw());
    }

    /**
     * Assert that the value of getRaw is null for newly created model.
     */
    public function testGetRawReturnsNullForANewlyCreatedModel(): void
    {
        self::assertNull($this->annuity->getRaw());
    }

    /**
     * Assert that setRaw sets the raw value.
     */
    public function testSetRawSetsTheValue(): void
    {
        $this->annuity->setRaw('{"key":"value"}');
        self::assertEquals('{"key":"value"}', $this->annuity->getRaw());
    }

    /**
     * Assert that setRaw sets the raw value.
     */
    public function testSetRawThrowsExceptionOnInvalidJson(): void
    {
        $this->expectException(JsonException::class);
        $this->annuity->setRaw('{"key:"value"}');
    }

    /**
     * Assert that the value of getCreatedAt is a string.
     */
    public function testGetCreatedAtReturnsInt(): void
    {
        $timeStamp = time();
        $annuity = new Annuity(
            $this->contextMock,
            $this->registryMock,
            $this->resourceMock,
            $this->resourceCollectionMock,
            ['created_at' => $timeStamp]
        );
        self::assertIsInt($annuity->getCreatedAt());
        self::assertEquals($timeStamp, $annuity->getCreatedAt());
    }

    /**
     * Assert that the value of getCreatedAt is null for newly created model.
     */
    public function testGetCreatedAtReturnsNullForANewlyCreatedModel(): void
    {
        self::assertNull($this->annuity->getCreatedAt());
    }

    /**
     * Assert that setCreatedAt sets the title.
     */
    public function testSetCreatedAtSetsTheValue(): void
    {
        $timeStamp = time();
        $this->annuity->setCreatedAt($timeStamp);
        self::assertEquals($timeStamp, $this->annuity->getCreatedAt());
    }

    /**
     * Assert that the value of getUpdatedAt is a string.
     */
    public function testGetUpdatedAtReturnsInt(): void
    {
        $timeStamp = time();
        $annuity = new Annuity(
            $this->contextMock,
            $this->registryMock,
            $this->resourceMock,
            $this->resourceCollectionMock,
            ['updated_at' => $timeStamp]
        );
        self::assertIsInt($annuity->getUpdatedAt());
        self::assertEquals($timeStamp, $annuity->getUpdatedAt());
    }

    /**
     * Assert that the value of getUpdatedAt is null for newly created model.
     */
    public function testGetUpdatedAtReturnsNullForANewlyCreatedModel(): void
    {
        self::assertNull($this->annuity->getUpdatedAt());
    }

    /**
     * Assert that setUpdatedAt sets the title.
     */
    public function testSetUpdatedAtSetsTheValue(): void
    {
        $timeStamp = time();
        $this->annuity->setUpdatedAt($timeStamp);
        self::assertEquals($timeStamp, $this->annuity->getUpdatedAt());
    }
}
