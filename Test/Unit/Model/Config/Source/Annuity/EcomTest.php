<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Test\Unit\Model\Config\Source\Annuity;

use PHPUnit\Framework\TestCase;
use Resursbank\Partpayment\Model\Config\Source\Annuity\Ecom;

/**
 * Unit tests for Model\Config\Source\Annuity\Ecom.
 */
class EcomTest extends TestCase
{
    /**
     * Verify that toOptionArray returns an empty array.
     *
     * @return void
     */
    public function testToOptionArray(): void
    {
        $ecom = new Ecom();
        static::assertEquals(
            expected: [],
            actual: $ecom->toOptionArray()
        );
    }

    /**
     * Verify that toArray returns an empty array.
     *
     * @return void
     */
    public function testToArray(): void
    {
        $ecom = new Ecom();
        static::assertEquals(
            expected: [],
            actual: $ecom->toArray()
        );
    }
}
