<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Test\Unit\Plugin\Core\Block\Adminhtml\System\Config;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Resursbank\Partpayment\Plugin\Core\Block\Adminhtml\System\Config\SupportInfo;
use Resursbank\Core\Block\Adminhtml\System\Config\SupportInfo as Subject;
use Resursbank\Partpayment\Helper\Log;
use Magento\Framework\Module\PackageInfo;

/**
 * Unit test for SupportInfo plugin.
 */
class SupportInfoTest extends TestCase
{
    /**
     * Verify that afterGetVersion properly appends version info to $result.
     *
     * @return void
     */
    public function testAfterGetVersion(): void
    {
        $version = '1.0.0';
        $packageInfo = $this->createMock(
            originalClassName: PackageInfo::class
        );
        $packageInfo->method('getVersion')
            ->willReturn(value: $version);
        $supportInfo = new SupportInfo(
            packageInfo: $packageInfo,
            log: $this->createMock(originalClassName: Log::class)
        );

        $inputResult = 'Resursbank_Core: 1.0.0';
        $expectedResult = $inputResult . '<br />Resursbank_Partpayment: ' .
            $version;
        static::assertEquals(
            expected: $expectedResult,
            actual: $supportInfo->afterGetVersion(
                subject: $this->createMock(
                    originalClassName: Subject::class
                ),
                result: $inputResult
            )
        );
    }

    /**
     * Verify that if an exception is thrown it is properly caught.
     *
     * @return void
     */
    public function testAfterGetVersionDoesNotThrow(): void
    {
        $packageInfo = $this->createMock(originalClassName: PackageInfo::class);
        $exception = new InvalidArgumentException(message: 'EXCEPTION');
        $packageInfo->method('getVersion')
            ->willThrowException(exception: $exception);
        $supportInfo = new SupportInfo(
            packageInfo: $packageInfo,
            log: $this->createMock(originalClassName: Log::class)
        );

        $inputResult = 'Resursbank_Core: 1.0.0';

        static::assertEquals(
            expected: $inputResult,
            actual: $supportInfo->afterGetVersion(
                subject: $this->createMock(originalClassName: Subject::class),
                result: $inputResult
            )
        );
    }
}
