<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Test\Unit\Plugin\Core\Helper;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use PHPUnit\Framework\TestCase;
use Resursbank\Partpayment\Plugin\Core\Helper\Ecom;
use Resursbank\Core\Helper\Ecom as Subject;
use Resursbank\Partpayment\Helper\Config;
use Resursbank\Core\Helper\Scope;
use Resursbank\Core\Helper\Version;

/**
 * Unit tests for Plugin\Core\Helper\Ecom.
 */
class EcomTest extends TestCase
{
    /**
     * Verify that result is modified when module is active.
     *
     * @return void
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function testAfterGetUserAgentWhenActive(): void
    {
        $version = '1.0.0';
        $inputResult = 'Resursbank_Core 1.0.0 |';
        $config = $this->createMock(originalClassName: Config::class);
        $config->method('isActive')
            ->willReturn(value: true);

        $versionObject =  $this->createMock(originalClassName: Version::class);
        $versionObject->method('getComposerVersion')
            ->willReturn($version);
        $ecom = new Ecom(
            config: $config,
            scope: $this->createMock(originalClassName: Scope::class),
            version: $versionObject
        );

        $expectedResult = $inputResult . ' Resursbank_Partpayment ' .
            $version . ' |';
        static::assertEquals(
            expected: $expectedResult,
            actual: $ecom->afterGetUserAgent(
                subject: $this->createMock(originalClassName: Subject::class),
                result: $inputResult
            )
        );
    }

    /**
     * Verify that result is not modified when module is inactive.
     *
     * @return void
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function testAfterGetUserAgentWhenNotActive(): void
    {
        $inputResult = 'Resursbank_Core 1.0.0 |';
        $config = $this->createMock(originalClassName: Config::class);
        $config->method('isActive')
            ->willReturn(value: false);

        $versionObject =  $this->createMock(originalClassName: Version::class);
        $versionObject->method('getComposerVersion')
            ->willReturn(value: '1.0.0');
        $ecom = new Ecom(
            config: $config,
            scope: $this->createMock(originalClassName: Scope::class),
            version: $versionObject
        );

        static::assertEquals(
            expected: $inputResult,
            actual: $ecom->afterGetUserAgent(
                subject: $this->createMock(originalClassName: Subject::class),
                result: $inputResult
            )
        );
    }
}
