<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Test\Unit\Plugin\Helper;

use PHPUnit\Framework\TestCase;
use Resursbank\Partpayment\Plugin\Helper\Api;
use Resursbank\Core\Helper\Version;
use Resursbank\Core\Helper\Api as Subject;

/**
 * Unit tests for Plugin\Helper\Api.
 */
class ApiTest extends TestCase
{
    /**
     * Verify afterGetUserAgent output.
     *
     * @return void
     */
    public function testAfterGetUserAgent(): void
    {
        $version = '1.0.0';
        $versionObject = $this->createMock(originalClassName: Version::class);
        $versionObject->method('getComposerVersion')
            ->willReturn(value: $version);

        $api = new Api(
            version: $versionObject
        );

        $inputResult = 'Resursbank_Core 1.0.0';
        $expectedResult = $inputResult . ' | Resursbank_Partpayment ' .
            $version;
        static::assertEquals(
            expected: $expectedResult,
            actual: $api->afterGetUserAgent(
                subject: $this->createMock(originalClassName: Subject::class),
                result: $inputResult
            )
        );
    }
}
