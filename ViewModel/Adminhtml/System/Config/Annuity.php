<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\ViewModel\Adminhtml\System\Config;

use Exception;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\ValidatorException;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Resursbank\Core\Api\Data\PaymentMethodInterface;
use Resursbank\Core\Helper\PaymentMethods;
use Resursbank\Core\Helper\Scope;
use Resursbank\Partpayment\Helper\Annuity as AnnuityHelper;
use Resursbank\Partpayment\Helper\Log;

/**
 * Render template containing data and methods to sync the list of available
 * annuity factors on-the-fly as payment method is being changed.
 */
class Annuity implements ArgumentInterface
{
    /**
     * @param PaymentMethods $paymentMethods
     * @param Log $log
     * @param AnnuityHelper $annuityHelper
     * @param PriceCurrencyInterface $priceCurrency
     * @param Scope $scope
     */
    public function __construct(
        private readonly PaymentMethods $paymentMethods,
        private readonly Log $log,
        private readonly AnnuityHelper $annuityHelper,
        private readonly PriceCurrencyInterface $priceCurrency,
        private readonly Scope $scope
    ) {
    }

    /**
     * Get part payment data.
     *
     * Retrieve JSON object containing all available annuity entities for all
     * available payment methods associated with the configured API credentials.
     *
     * @return string
     */
    public function getPartpaymentData(): string
    {
        $result = '';

        try {
            $result = json_encode(
                $this->getMethods(),
                JSON_THROW_ON_ERROR
            );
        } catch (Exception $e) {
            $this->log->exception($e);
        }

        return $result;
    }

    /**
     * Get methods.
     *
     * This method compiles a list of annuity entities associated with active
     * payment methods, which are associated with the currently configured API
     * account. It also includes min / max order total values.
     *
     * @return array<int, array>
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws ValidatorException
     */
    private function getMethods(): array
    {
        $result = [];

        $methods = $this->paymentMethods
            ->getMethodsByCredentials(
                $this->scope->getId(),
                $this->scope->getType()
            );

        foreach ($methods as $method) {
            $methodId = $method->getMethodId();

            if ($methodId !== null) {
                $result[] = [
                    'id' => $methodId,
                    'minPrice' => $this->priceCurrency->convertAndFormat(
                        (float)$method->getMinOrderTotal(),
                        false
                    ),
                    'maxPrice' => $this->priceCurrency->convertAndFormat(
                        (float)$method->getMaxOrderTotal(),
                        false
                    ),
                    'annuityList' => $this->getAnnuityList($method)
                ];
            }
        }

        return $result;
    }

    /**
     * Get annuity list.
     *
     * Retrieve simple list of annuity entities associated with supplied
     * payment method.
     *
     * @param PaymentMethodInterface $method
     * @return array<int, array<string, int|string>>
     */
    private function getAnnuityList(
        PaymentMethodInterface $method
    ): array {
        $result = [];

        $annuities = $this->annuityHelper->getListByMethod($method);

        foreach ($annuities as $annuity) {
            $annuityId = $annuity->getAnnuityId();
            $title = $annuity->getTitle();

            if ($annuityId !== null && $title !== null) {
                $result[] = [
                    'id' => $annuityId,
                    'title' => $title
                ];
            }
        }

        return $result;
    }
}
