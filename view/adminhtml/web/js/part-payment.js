/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

// phpcs:ignoreFile
define(
    [
        'jquery'
    ],
    function ($) {
        'use strict';

        /**
         * @typedef {object} RbPp.Annuity
         * @property {number} id
         * @property {string} title
         */

        /**
         * @typedef {object} RbPp.MethodData
         * @property {number} id
         * @property {string} minPrice
         * @property {string} maxPrice
         * @property {RbPp.Annuity[]} annuityList
         */

        /**
         * States whether initialization has happened.
         *
         * @type {boolean}
         */
        var initialized = false;

        /**
         * Data containing payment methods, min / max prices and annuity lists.
         *
         * @type {RbPp.MethodData[]}
         * @readonly
         */
        var methodDataList = [];

        /**
         * Annuity select box.
         *
         * @type {HTMLSelectElement|undefined}
         */
        var methodBox = $(
            '#payment_other_resursbank_section_partpayment_method'
        ).get(0);

        /**
         * Annuity select box.
         *
         * @type {HTMLSelectElement|undefined}
         */
        var annuityBox = $(
            '#payment_other_resursbank_section_partpayment_annuity'
        ).get(0);

        /**
         * Element displaying minimum value for part payment price to apply.
         *
         * @type {HTMLDivElement|undefined}
         */
        var minPriceField = $('#resursbank_partpayment_min_price').get(0);

        /**
         * Element displaying maximum value for part payment price to apply.
         *
         * @type {HTMLDivElement|undefined}
         */
        var maxPriceField = $('#resursbank_partpayment_max_price').get(0);

        /**
         * Event handler for when the payment method changes.
         */
        function onChangeMethod() {
            updateAnnuityOptions();
            updateMinPrice();
            updateMaxPrice();
        }

        /**
         * Updates the list of available annuities for the selected payment
         * method.
         */
        function updateAnnuityOptions() {
            /** @type {HTMLOptionElement|null} */
            var firstOption = null;

            /** @type {RbPp.MethodData|null} */
            var methodData = null;

            /** @type {HTMLOptionElement[]} */
            var options;

            if (annuityBox instanceof HTMLSelectElement) {
                firstOption = annuityBox.options.item(0);
                methodData = getSelectedMethodData();
                options = methodData !== null ?
                    methodData.annuityList.map(function (annuity) {
                        return new Option(annuity.title, String(annuity.id));
                    }) : [];

                if (firstOption instanceof HTMLOptionElement) {
                    options.unshift(firstOption);
                }

                $(annuityBox).empty().append(options);
            }
        }

        /**
         * Updates the value of the minimum price field.
         */
        function updateMinPrice() {
            var methodData = getSelectedMethodData();
            var val = '';

            if (methodData !== null &&
                typeof methodData.minPrice === 'string'
            ) {
                val = methodData.minPrice;
            }

            $(minPriceField).text(val);
        }

        /**
         * Updates the value of the maximum price field.
         */
        function updateMaxPrice() {
            var methodData = getSelectedMethodData();
            var val = '';

            if (methodData !== null &&
                typeof methodData.maxPrice === 'string'
            ) {
                val = methodData.maxPrice;
            }

            $(maxPriceField).text(val);
        }

        /**
         * @returns {RbPp.MethodData|null}
         */
        function getSelectedMethodData() {
            var methodId = getSelectedMethodId();

            return methodId !== null ? getMethodData(methodId) : null;
        }

        /**
         * Returns the payment method data that is associated with the given
         * payment method ID.
         *
         * @param {number} methodId
         * @return {RbPp.MethodData|null}
         */
        function getMethodData(methodId) {
            var result = null;
            var index = -1;

            methodDataList.forEach(function (methodData, i) {
                if (methodData.id === methodId) {
                    index = i;
                }
            });

            if (index > -1) {
                result = methodDataList[index];
            }

            return result;
        }

        /**
         * @return {number|null}
         */
        function getSelectedMethodId() {
            var id = parseInt($(methodBox).val());

            return typeof id === 'number' && id > 0 ? id : null;
        }

        /**
         * @namespace RbPp
         * @constant
         */
        var EXPORT = {
            /**
             * Initialization function. Can only be run once.
             *
             * @param {object} config
             * @param {RbPp.MethodData[]} config.methodDataList
             */
            init: function (config) {
                if (initialized === false) {
                    if (Array.isArray(config.methodDataList)) {
                        methodDataList = config.methodDataList.slice();
                    }

                    $(methodBox).on('change', onChangeMethod);

                    initialized = true;
                }

                return EXPORT;
            }
        };

        return Object.freeze(EXPORT);
    }
);
