/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

require([
    'jquery',
    'Magento_Ui/js/modal/alert',
    'mage/translate',
    'domReady!'
], function ($, alert, $t) {
    if (typeof Resursbank_GetPeriods !== 'function') {
        return;
    }

    // Skip this if we are not on the payment section of the admin config.
    if (!$('#payment_other_resursbank_section_partpayment_ecom_method').length) {
        return;
    }

    Resursbank_GetPeriods.generate({
        errorHandler: function (message) {
            alert({
                title: 'Resurs Bank',
                content: message
            });
        }
    });
});
