// Globally cached instance of the Resursbank_PartPayment widget, so we can
// reload the observers when the DOM changes.
let RB_PP_WIDGET_INSTANCE = null;

/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */
require([
    'jquery',
    'mage/translate',
    'mage/cookies', // Required for getCustomRequestData() to function.
    'domReady!'
], function ($, $t, cookies) {
    if (typeof Resursbank_PartPayment !== 'function') {
        return;
    }

    /**
     * Create a new instance of the Resursbank_PartPayment widget.
     *
     * Note that parsing price information from the amount element is an active
     * choice for the following reasons:
     *
     * 1. Obtaining current price information based on product configuration
     * works differently for different product types.
     * 2. Price adjustments applied by third party modules may not be reflected
     * in the data stored in the priceBox.
     * 3. It was unclear if we even could resolve price information for simple
     * products using custom options because of how the data is stored by that
     * module.
     *
     * Resolving the data from a JS object would be better but this is probably
     * more stable and versatile.
     */
    const overrides = {
        /**
         * When we change the configuration in the add-to-cart form, update
         * the price information of the widget.
         */
        getObservableElements: function() {
            const formElement = document.querySelector(
                "#product_addtocart_form"
            );
            const priceElement = document.querySelector(
                "#rb-pp-widget-container [data-role='priceBox']"
            );

            const result = [formElement, priceElement];

            if (result.length < 1) {
                throw new Error(
                    $t('Failed to obtain element(s) observable for changes.')
                );
            }

            return result;
        },

        /**
         * Resolve element we will get price data from.
         */
        getAmountElement: function() {
            // We use XML to move the price element inside our
            // #rb-pp-widget-container, for that reason this selector should be
            // fine regardless of how a third-party theme may affect the DOM.
            // For bundled products the price element is located in the
            // #product_addtocart_form element instead, which should also always
            // be present regardless of theme.
            const domPath = RB_PP_PRODUCT_TYPE !== 'bundle' ?
                '#rb-pp-widget-container [data-role="priceBox"] .price' :
                '#product_addtocart_form [data-role="priceBox"] .price';

            const el = document.querySelector(domPath);

            if (el === null) {
                throw new Error(
                    $t('Failed to obtain amount element.')
                );
            }

            return el;
        },

        /**
         * Resolve element we will get qty data from.
         */
        getQtyElement: function() {
            const el = document.querySelector('#product_addtocart_form [name="qty"]');

            if (el === null) {
                throw new Error(
                    $t('Failed to obtain quantity element.')
                );
            }

            return el;
        },

        /**
         * We must include the form_key with POST requests.
         */
        getCustomRequestData: function() {
            return {
                form_key: $.mage.cookies.get('form_key')
            };
        },

        /**
         * Override content-type for AJAX request. Magento does not support
         * application/json out of the box in frontend controllers, and we
         * need to include the form_key in the request to support cross-site
         * request forgery protection.
         *
         * @returns {string}
         */
        getRequestContentType: function() {
            return 'application/x-www-form-urlencoded';
        },

        /**
         * Override body for AJAX request. Magento does not support
         * application/json out of the box in frontend controllers, and we
         * need to include the form_key in the request to support cross-site
         * request forgery protection.
         *
         * @returns {string}
         */
        getRequestBody: function(amount) {
            return new URLSearchParams(this.getRequestData(amount)).toString();
        }
    };

    // We need to pick up amount in a special way for grouped products.
    if (RB_PP_PRODUCT_TYPE === 'grouped') {
        overrides.getAmount = function() {
            // Get all TR elements in #super-product-table.
            const trElements = document.querySelectorAll(
                '#super-product-table tbody tr'
            );

            let result = 0;

            trElements.forEach((tr) => {
                // Get the price element in the current TR.
                const priceElement = tr.querySelector("[data-role='priceBox'] .price");

                // If we cannot find a price element, skip this TR.
                if (priceElement === null) {
                    return;
                }

                // Get price from innerHTML.
                const price = Resursbank_PartPayment.parseFromContentToFloat(
                    priceElement.innerHTML
                );

                // Get qty element in the current TR.
                const qtyElement = tr.querySelector("input.qty");

                // If we cannot find a qty element, use 1 as default.
                const qty = qtyElement === null ?
                    0 :
                    Resursbank_PartPayment.parseFromContentToFloat(qtyElement.value);

                result += price * qty;
            });

            return result;
        };
    }

    // Store instance in local variable for later use.
    RB_PP_WIDGET_INSTANCE = Resursbank_PartPayment.createInstance(
        document.getElementById('rb-pp-widget-container'),
        overrides
    );

    // Reload instance when the DOM of #product_addtocart_form changes.
    const addtocartForm = document.getElementById(
        'product_addtocart_form'
    );
    const observer = new MutationObserver(() => {
        RB_PP_WIDGET_INSTANCE.reloadElementObservers();
    });

    observer.observe(
        addtocartForm, {
            childList: true,
            subtree: true,
            attributes:  true,
            characterData: true
        }
    );
    addtocartForm._mutationObserver = observer;
});
