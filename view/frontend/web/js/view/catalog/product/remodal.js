/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

define(
    [
        'jquery',
        'ko',
        'mage/translate',
        'Magento_Catalog/js/price-utils',
        'Resursbank_Core/js/view/remodal',
        'Resursbank_Partpayment/js/model/part-payment'
    ],
    /**
     * @param $
     * @param ko
     * @param $t
     * @param PriceUtils
     * @param Component
     * @param {RbPp.Model.PartPayment} PartPaymentModel
     * @returns {*}
     */
    function (
        $,
        ko,
        $t,
        PriceUtils,
        Component,
        PartPaymentModel
    ) {
        'use strict';

        return Component.extend({
            initialize: function () {
                var me = this;

                me._super();

                PartPaymentModel.finalPrice.subscribe(function () {
                    me.update(true);
                });
            }
        });
    }
);
